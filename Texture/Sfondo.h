#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include "CGUtils/include/tga_io.h"
#include "CGUtils/include/3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>

extern double width, height;

static GLuint texName;

//render dello sfondo finestra principale
void sfondoRender(){
	
	//imposta matrice, punto di vista e texture
	glViewport(0,0,width,height);                      //disegno utilizzando tutta la finestra
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45,(double)(width) / ((double)(height)),1,500);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0,0,-300,          //eye
			  0,0,0,            //center
			  0,-1,0);           //up
	
	
	glPushMatrix();		  
	//sfondo
	glEnable(GL_TEXTURE_2D);
	//incolla la texture come un'adesivo al poligono che traccio
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	
	glBindTexture(GL_TEXTURE_2D,texName);
	
	//glTexCoord2f -> seleziona la porzione dell'immagine che voglio 
	//utilizzare come texture
	//glVertex3f -> disegno il poligono su cui verr� incollata la texture
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-width, -height, 0.0);
	glTexCoord2f(0.0, 20.0); glVertex3f(-width, height, 0.0);
	glTexCoord2f(5.0, 20.0); glVertex3f(width, height, 0.0);
	glTexCoord2f(5.0, 0.0); glVertex3f(width, -height, 0.0);
	glEnd();
	
	glDisable(GL_TEXTURE_2D);
}

//sfondo finestra principale
void sfondo(){
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glShadeModel(GL_FLAT);
	glEnable(GL_DEPTH_TEST);
	
	//texture
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glGenTextures(1,&texName);
	glBindTexture(GL_TEXTURE_2D,texName);
	
	//caricamento immagine e definisco parametri texture
	CGU::TGAImg sfondo;
	sfondo.load("stelle.tga");
	
	
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,sfondo.width(),sfondo.height(),0,GL_RGBA,GL_UNSIGNED_BYTE,sfondo.pixels());
	
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
	
	
	sfondoRender();
}


//-----------------------Fine finestra principale----------------------
