#include <cstdlib>
#include <cstdio>
#include <GL/glut.h>
#include "tga_io.h"
#include "3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>

//variabili per il caricamento delle texture
static GLuint texNameL;	

extern int widthL, heightL;

//sfondo finestra legenda
void sfondoLegenda(){
	
	//caricamento immagine e definisco parametri texture
	CGU::TGAImg sfondoL;
	sfondoL.load("stelle.tga");
	//texture
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	glGenTextures(1,&texNameL);
	glBindTexture(GL_TEXTURE_2D,texNameL);
	
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,sfondoL.width(),sfondoL.height(),0,GL_RGBA,GL_UNSIGNED_BYTE,sfondoL.pixels());
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
}


//render dello sfondo della legenda
void sfondoRenderLegenda(){
	
	//imposta matrice, punto di vista e texture
	glViewport(0,0,widthL,heightL);                      //disegno utilizzando tutta la finestra
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45,(double)(widthL) / ((double)(heightL)),1,71);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0,0,-70,          //eye
			  0,0,0,            //center
			  0,-1,0);           //up
	//sfondo
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
	glBindTexture(GL_TEXTURE_2D,texNameL);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-100.0, -100.0, 0.0);
	glTexCoord2f(0.0, 100.0); glVertex3f(-100.0, 100.0, 0.0);
	glTexCoord2f(100.0, 100.0); glVertex3f(100.0, 100.0, 0.0);
	glTexCoord2f(100.0, 0.0); glVertex3f(100.0, -100.0, 0.0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

//--------------------------Fine finestra legenda------------------------
