#include <cstdlib>
#include <cstdio>
#include <GL/glut.h>
#include "tga_io.h"
#include "3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>

extern double widthL, heightL;
extern bool legenda_flag;
void *fontL = GLUT_BITMAP_HELVETICA_12;


void renderBitmapStringL(
						float x,
						float y,
						float z,
						void *font,
						char *string) {
	glColor3f(1.0,1.0,1.0);
	char *c;
	glRasterPos3f(x, y,z);
	for (c=string; *c != '\0'; c++) {
		glutBitmapCharacter(fontL, *c);
	}
}

void testoLegenda(){
	
	char stop[10] = "s -> STOP",go[8] = "g -> GO",f1[13] = "FSU -> zoom+";
	char exit[10] = "q -> EXIT",meno[10] = "- -> <<",piu[10] = "+ -> >>";
	char f2[14] = "FGIU -> zoom-",f3[13] = "FSIN -> rotL",f4[13] = "FDES -> rotR";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
	renderBitmapStringL(20.0,-20.0,0.0,(void *)fontL,stop);
	renderBitmapStringL(20.0,-15.0,0.0,(void *)fontL,go);
	renderBitmapStringL(20.0,-10.0,0.0,(void *)fontL,piu);
	renderBitmapStringL(20.0,-5.0,0.0,(void *)fontL,meno);
	renderBitmapStringL(20.0,0.0,0.0,(void *)fontL,exit);
	renderBitmapStringL(20.0,5.0,0.0,(void *)fontL,f1);
	renderBitmapStringL(20.0,10.0,0.0,(void *)fontL,f2);
	renderBitmapStringL(20.0,15.0,0.0,(void *)fontL,f3);
	renderBitmapStringL(20.0,20.0,0.0,(void *)fontL,f4);
	
}

void proiezioneLuceLegenda(){
     
	//imposta matrice, punto di vista e paramentri luce per i Pianeti
	glViewport(0,0,widthL,heightL);                      //disegno utilizzando tutta la finestra
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60,(double)(widthL) / ((double)(heightL)),1,110);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//il terzo parametro di EYE specifica la distanza del punto di vista
	//rispetto all'asse Z
	gluLookAt(0,0,45,          //eye
			  0,0,0,           //center
			  0,-1,0);		   //up
	glPushMatrix();
	if(legenda_flag)
		testoLegenda();
   	glFlush();         
	
}



