#include <cstdlib>
#include <cstdio>
#include <GL/glut.h>
#include "tga_io.h"
#include "3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>

#define GAP  25             /* gap between subwindows */

//namespace
using namespace std;
using namespace CGU;

extern GLuint finestraGen;

GLuint View1;
GLuint sub_width = 256, sub_height = 256;


void reshapeSW(int width,  int height);

void View1Display();

void ResetViewport();

void subWindows()
{
	
	//World Window and Display
	View1 = glutCreateSubWindow(finestraGen, GAP, GAP, 256, 256);	
	glutDisplayFunc(View1Display);
	glutReshapeFunc(reshapeSW);

}

//Background Window Setting
void reshapeSW(int width,  int height) 
{
	//main view setting
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, width, height, 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	sub_width = width;
	sub_height = height;

	//View1 Display
	glutSetWindow(View1);
	glutPositionWindow(GAP, GAP);
	glutReshapeWindow(sub_width, sub_height);	
}



//View1Display
void View1Display(){

	
	//viewport rest;
	ResetViewport();

	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(1.0, 1.0, 1.0);
	glPushMatrix();
	gluLookAt(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glPopMatrix();
	glutSwapBuffers();
}


void ResetViewport()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-2.0, 2.0, -2.0, 2.0, 0.5, 5.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
