/*Header file per la gestione della camera
cambio di visuale con i tasti della tastiera*/

#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include "CGUtils/include/tga_io.h"
#include "CGUtils/include/3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>

//namespace
using namespace std;
using namespace CGU;

//flag
static bool stop_flag = false;							            //flag per fermare le rivoluzioni/rotazioni
static bool tasto_sinistro, tasto_destro, tasto_su, tasto_giu;		//variabili associate alle freccie della tastiera
																	//(true = premuto
																	
															
static double yRot = 0;												//variabile per rotazione
																
static float velocita = 0.1;										//velocità avanzamento camera

																	
																
static Camera camera;												//variabile della camera

//CAMERA
void visualeCamera(){
	
	if (tasto_giu) {
		camera.position.x += -camera.frame.Zaxis.x * velocita;
		camera.position.y += -camera.frame.Zaxis.y * velocita;
		camera.position.z += -camera.frame.Zaxis.z * velocita;
	}
	
	if (tasto_su) {
		camera.position.x += camera.frame.Zaxis.x * velocita;
		camera.position.y += camera.frame.Zaxis.y * velocita;
		camera.position.z += camera.frame.Zaxis.z * velocita;
	}
	
	Matrix4x4 c;
	buildCameraMatrix(camera,c);
	glMultMatrixf(c.GL_array());
}

//spostamento camera e zoom
void pressioneTasti(int key, int x, int y){
	
	if (stop_flag) {
		switch (key){
				
			case GLUT_KEY_UP:
				tasto_su = true;
				break;            
			case GLUT_KEY_LEFT:
				yRot --;
				break;         
			case GLUT_KEY_RIGHT:
				yRot ++;
				break;         
			case GLUT_KEY_DOWN:
				tasto_giu = true;
				break;         
		}
	}
}

void rilascioTasti(int key, int x, int y){
	
	switch (key){
			
		case GLUT_KEY_UP:
			tasto_su = false;
			break;            
		case GLUT_KEY_DOWN:
			tasto_giu = false;
			break;         
    }
}
//------------------------Fine CAMERA----------------------
