/*Header file per la definizione dei men�
attivabili con il tasto destro del mouse
*/

#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>
#include "CGUtils/include/3dmath.h"


//costanti per il cambio di luce
#define LUCE1 1
#define LUCE2 2
#define LUCE3 3
#define LUCE4 4
#define LUCE5 5
#define LUCE6 6

//costanti per la visualizzazione dei nomi dei pianeti
#define NOMION 1
#define NOMIOFF 2

//costanti per la visualizzazione della legenda
#define LEGENDAON 1
#define LEGENDAOFF 2

//namespace
using namespace std;
using namespace CGU;

//flag per la visualizzazione dei nomi
extern bool nomi_flag, legenda_flag;

//ID men�
int menuLu, menuGen, menuNome,menuLeg;


//flag per il men�
bool menu_flag;

//MENU'

//stato del men�
void statoMenu(int stato, int x, int y){
	
	if (stato == GLUT_MENU_IN_USE)
		menu_flag = true;
	else
		menu_flag = false;
}

//men� generale
void menuGenerale(int opzione){
}

//men� per il cambio di luce
void menuLuce(int opzione){
	
	switch (opzione) {
		case LUCE1:
			glDisable(GL_LIGHTING);
			glEnable(GL_LIGHT1);
			glEnable(GL_LIGHTING);
			break;
		case LUCE2:
			glDisable(GL_LIGHTING);
			glEnable(GL_LIGHT2);
			glEnable(GL_LIGHTING);
			break;
		case LUCE3:
			glDisable(GL_LIGHTING);
			glEnable(GL_LIGHT3);
			glEnable(GL_LIGHTING);
			break;
		case LUCE4:
			glDisable(GL_LIGHTING);
			glEnable(GL_LIGHT4);
			glEnable(GL_LIGHTING);
			break;
		case LUCE5:
			glDisable(GL_LIGHTING);
			glEnable(GL_LIGHT5);
			glEnable(GL_LIGHTING);
			break;
		case LUCE6:
			glDisable(GL_LIGHTING);
			glEnable(GL_LIGHT6);
			glEnable(GL_LIGHTING);
			break;
	}
}

//menù per attivare/disattivare i nomi dei pianeti
void menuNomi(int opzione){
	
	switch (opzione) {
		case NOMION:
			nomi_flag = true;
			break;
		case NOMIOFF:
			nomi_flag = false;
			break;
	}
}

//menù per attivare/disattivare la visualizzazione della legenda
void menuLegenda(int opzione){
	
	switch (opzione) {
		case LEGENDAON:
			legenda_flag = true;
			break;
		case LEGENDAOFF:
			legenda_flag = false;
			break;
	}
}

void popUpMenu(){
	
	menuNome = glutCreateMenu(menuNomi);
	
	glutAddMenuEntry("S�",NOMION);
	glutAddMenuEntry("No",NOMIOFF);
	
	menuLu = glutCreateMenu(menuLuce);
	
	glutAddMenuEntry("Luce 1",LUCE1);
	glutAddMenuEntry("Luce 2",LUCE2);
	glutAddMenuEntry("Luce 3",LUCE3);
	glutAddMenuEntry("Luce 4",LUCE4);
	glutAddMenuEntry("Luce 5",LUCE5);
	glutAddMenuEntry("Luce 6",LUCE6);
	
	menuGen = glutCreateMenu(menuGenerale);
	
	glutAddSubMenu("Tipo luce",menuLu);
	glutAddSubMenu("Visualizzazione nomi pianeti",menuNome);
	
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	
	glutMenuStatusFunc(statoMenu);
}

void popUpMenuLegenda(){
	
	menuLeg = glutCreateMenu(menuLegenda);
	
	glutAddMenuEntry("S�",LEGENDAON);
	glutAddMenuEntry("No",LEGENDAOFF);
	
	menuGen = glutCreateMenu(menuGenerale);
	
	glutAddSubMenu("Visualizzazione legenda",menuLeg);
	
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	
	glutMenuStatusFunc(statoMenu);
}
//---------------------------Fine MENU'--------------------------------
