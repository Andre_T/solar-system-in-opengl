/*Header file per l'implementazione di 
informazioni visualizzata nella finestra
in real time*/


#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>
#include "CGUtils/include/3dmath.h"


//namespace
using namespace std;
using namespace CGU;

//variabili per FPS
static int frame;
GLint tempo,tempoBase;
static char s[50];

void *fontI = GLUT_BITMAP_HELVETICA_12;

extern double width, height;

void renderBitmapStringInfo(
						float x,
						float y,
						float z,
						void *font,
						char *string) {
	glColor3f(1.0,1.0,1.0);
	char *c;
	glRasterPos3f(x, y,z);
	for (c=string; *c != '\0'; c++) {
		glutBitmapCharacter(fontI, *c);
	}
}


//visualizza i FPS nella finestra
void fps(){
	
	frame++;
	tempo = glutGet(GLUT_ELAPSED_TIME);
	if (tempo - tempoBase > 1000) {
		sprintf(s,"FPS:%4.2f",frame*1000.0/(tempo - tempoBase));
		tempoBase = tempo;
		frame = 0;
	}

	//posizione del contatore dei frame
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0,width,height,0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	//deve definire il comportamente del oggetto 
	//nei confronti dell'illuminazione
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapStringInfo(20,20,0,fontI,s);
	
	
	
	glPopMatrix();
}
