/*Header file per la definizione dei 
parametri del materiale di ogni pianeta
*/

#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>
#include "CGUtils/include/3dmath.h"


//namespace
using namespace std;
using namespace CGU;

//MATERIALI DEI PIANETI

//imposta il materiale del Sole
void materialeSole(){
	
	GLfloat front_mat_emission[] = {1.0,1.0,0.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}

//imposta il materiale per la Terra
void materialeTerra(){
	
	GLfloat front_mat_specular[] = {0.5,0.5,0.5,1.0};
	GLfloat front_mat_diffuse[] = {0.2,0.2,1.0,1.0};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,1.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}



//imposta il materiale per Giove
void materialeGiove(){
	
	GLfloat front_mat_specular[] = {0.5,1.8,1.0,1.0};
	GLfloat front_mat_diffuse[] = {3.0,1.5,0.2,3.5};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,2.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}



//imposta il materiale per Marte
void materialeMarte(){
	
	GLfloat front_mat_specular[] = {2.5,4.8,5.0,1.0};
	GLfloat front_mat_diffuse[] = {2.0,0.5,0.3,4.5};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,3.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}



//imposta il materiale per Venere
void materialeVenere(){
	
	GLfloat front_mat_specular[] = {8.5,4.8,5.0,4.0};
	GLfloat front_mat_diffuse[] = {5.0,3.5,5.3,7.5};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,3.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}



//imposta il materiale per Saturno
void materialeSaturno(){
	
	GLfloat front_mat_specular[] = {4.5,1.8,6.0,1.0};
	GLfloat front_mat_diffuse[] = {4.0,5.5,2.0,0.5};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,1.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}


//imposta il materiale del pianeta
void materialePlutone(){
	
	GLfloat front_mat_specular[] = {2.5,4.8,5.0,4.0};
	GLfloat front_mat_diffuse[] = {2.0,3.5,5.3,7.5};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,3.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}



//imposta il materiale per le Lune
void materialeLune(){
	
	GLfloat front_mat_specular[] = {1.0,1.0,1.0,1.0};
	GLfloat front_mat_diffuse[] = {1.0,1.0,1.0,1.0};
	GLfloat front_mat_shininess[] = {50.0};
	GLfloat front_mat_emission[] = {0.0,0.0,0.0,1.0};
	
	glMaterialfv(GL_FRONT, GL_DIFFUSE, front_mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, front_mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, front_mat_shininess);
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
}

//---------------------------Fine MATERIALI E NOMI----------------------
