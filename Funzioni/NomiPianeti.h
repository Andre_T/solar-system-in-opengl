/*Header file per la stampa dei nomi dei 
pianeti, se attivato il relativo men�
*/

#include <cstdlib>
#include <cstdio>
#include <GL/glut.h>
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>
#include "3dmath.h"


//namespace
using namespace std;
using namespace CGU;

//font nomi pianeti
void *font = GLUT_BITMAP_HELVETICA_12;

//posizione la scritta sopra l'oggetto
void renderBitmapString(
						float x,
						float y,
						float z,
						void *font,
						char *string) {
	glColor3f(1.0,1.0,1.0);
	char *c;
	glRasterPos3f(x, y,z);
	for (c=string; *c != '\0'; c++) {
		glutBitmapCharacter(font, *c);
	}
}

//stampa il nome del pianeta
void nomeSole(){
	
	char nome[5] = "Sole";

	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-7.0,0.0,(void *)font,nome);
}

//stampa il nome del pianeta
void nomeMercurio(){
	
	char nome[9] = "Mercurio";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-3.0,0.0,(void *)font,nome);
}

//stampa il nome del pianeta
void nomeTerra(){
	
	char nome[6] = "Terra";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-3.0,0.0,(void *)font,nome);
}

//stampa il nome del pianeta
void nomeGiove(){
	
	char nome[6] = "Giove";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-6.0,0.0,(void *)font,nome);
}

//stampa il nome del pianeta
void nomeMarte(){
	
	char nome[6] = "Marte";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-3.0,0.0,(void *)font,nome);
}

//stampa il nome del pianeta
void nomeVenere(){
	
	char nome[7] = "Venere";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-3.0,0.0,(void *)font,nome);
}

void nomeSaturno(){
	
	char nome[8] = "Saturno";
	
	//stampa il nome del pianeta
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-3.0,0.0,(void *)font,nome);
}

//stampa il nome del pianeta
void nomePlutone(){
	
	char nome[8] = "Plutone";
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,1.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	renderBitmapString(0.0,-3.0,0.0,(void *)font,nome);
}
