#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include "Menu.h"
#include "InfoRT.h"
#include "Camera.h"
#include "Pianeti.h"
#include "FunzioniL.h"
#include "Sfondo.h"
#include "CGUtils/include/tga_io.h"
#include "CGUtils/include/3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>


//namespace
using namespace std;
using namespace CGU;

double width, height, widthL = 256, heightL = 256;

float anno = 0, giorno = 0;			//variabili per la modifica della velocità di animazione

bool nomi_flag = false, legenda_flag = true;

//ID finestre
int finestraGen, legenda;

//metodi
void proiezioneLucePianeti(void);
void luce(void);


//VELOCITA' ANIMAZIONE
void keyboard(unsigned char key,int x,int y){
	
	switch (key) {
		case 'q':
			exit(EXIT_SUCCESS);
			break;
		case '+':
			giorno = giorno*10;
			break;
		case '-':
			giorno = giorno/10;
			break;
		case 's':
			stop_flag = true;
			break;
		case 'g':
			stop_flag = false;
			break;
	}
}
//----------------------------------------------------------


//render legenda
void displayLegenda(){
	
	glutSetWindow(legenda);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glClearColor(0.0, 0.0, 0.0, 0.0);
	
	proiezioneLuceLegenda();
	
	GLfloat front_mat_emission[] = {1.0,1.0,1.0,0.0};
	glMaterialfv(GL_FRONT, GL_EMISSION, front_mat_emission);
	
	glutSwapBuffers();
	glFlush();
	
}

//FINESTRA PRINCIPALE

//ridisegno forzato finestra principale
void idle(void){
	
	glutSetWindow(finestraGen);
	
	glutPostRedisplay();
	
	if (!stop_flag) {
		giorno += 0.05;
		if (giorno == 365)
			giorno = 0;
		anno += 0.05;
		if (anno == 10000)
			anno = 0;
	}
	
	glutSetWindow(legenda);
	glutPostRedisplay();
}

//inizializza finestra principale
void init(void){
	
	glutSetWindow(finestraGen);
	
	glClearColor(0.0,0.0,0.0,0.0);
	glEnable(GL_DEPTH_TEST);
	
	//inizializzo le variabili associate alle freccie
	tasto_sinistro = false;
	tasto_destro = false;
	tasto_su = false;
	tasto_giu = false;
	
	//posizione iniziale camera
	camera.position.set(3,3,10);
	
	luce();
	
	glutKeyboardFunc(keyboard);				//abilito input da tastiera
	glutSpecialFunc(pressioneTasti);		//cambio visuale da tastiera (pressione tasti)
	glutSpecialUpFunc(rilascioTasti);		//cambio visuale da tastiera (rilascio tasti)
	
}

//Resize della finestra principale e della sotto-finestra
//Va messo tutto insieme!!! altrimenti non ridimensiona la finestra
void reshape(int w,int h){
	
	glutSetWindow(finestraGen);
	
	width = w;
	height = h;
	
	glutSetWindow(legenda);

	glutReshapeWindow(widthL,heightL);
	
}

//LUCI

//imposta la luce delle finestra
void luce(){
	
	//se non viene cambiata, la luce � la numero 6
	glEnable(GL_LIGHT6);
	
	
	glEnable(GL_LIGHTING);
	
	
	//imposto i paramentri della luce 6
	GLfloat light0_ambiente[] = {0.2,0.2,0.2,1.0};
	GLfloat light0_diffusione[] = {1.0,1.0,1.0,1.0};
	GLfloat light0_riflessione[] = {1.0,1.0,1.0,1.0};
	
	glLightfv(GL_LIGHT6, GL_AMBIENT, light0_ambiente);
	glLightfv(GL_LIGHT6, GL_DIFFUSE, light0_diffusione);
	glLightfv(GL_LIGHT6, GL_SPECULAR, light0_riflessione);
}

//imposta la prospettiva e il punto di vista della luce
void proiezioneLucePianeti(){
	
	//imposta matrice, punto di vista e paramentri luce per i Pianeti
	glViewport(0,0,width,height);                      //disegno utilizzando tutta la finestra
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60,(double)(width) / ((double)(height)),1,110);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0,0,-70,          //eye
			  0,0,0,            //center
			  0,-1,0);           //up
	
	//rotazione camera
	if (stop_flag)
		glRotatef(yRot,0,1,0);
		
	//posizione la luce nella stessa posizione del Sole
	GLfloat light0_posizione[] = {0.0,0.0,0.0,1.0};
	glLightfv(GL_LIGHT6, GL_POSITION, light0_posizione);
	
}

//DISEGNA FINESTRA PRINCIPALE

//disegna il sistema solare
void sistemaSolare(void){
	
	glutSetWindow(finestraGen);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	sfondo();
	
	
	proiezioneLucePianeti();			//attivo la luce (proviene dal Sole)
	visualeCamera();					//attivo la camera
	
	
	
	glShadeModel(GL_SMOOTH);
	
	Sole();

	//salvo la possibilità di disegnare attorno al Sole
	glPushMatrix();
	glPushMatrix();
	glPushMatrix();
	glPushMatrix();
	glPushMatrix();
	glPushMatrix();

	Mercurio();

	glPopMatrix();
	glPopMatrix();
	
	Marte();

	glPopMatrix();
	glPopMatrix();					//ripristino il disegno attorno al Sole
	
	Venere();
	
	glPopMatrix();
	glPopMatrix();					//ripristino il disegno attorno al Sole
	
	Terra();
	
	glPopMatrix();
	
	lunaTerra();

	glPopMatrix();					//ripristino il disegno attorno al Sole
	
	Giove();

	glPopMatrix();                    //torno a disegnare attorno a Giove

	lunaGiove1();

	glPopMatrix();	
	
	lunaGiove2();					//ripristino il disegno attorno al Sole
	
	glPopMatrix();
	
	lunaGiove3();

	glPopMatrix();							//ripristino il disegno attorno al Sole
	
	Saturno();
	//disegno gli anelli di Saturno
	anelli();
	
	glPopMatrix();							//torno a disegnare attorno a Saturno
	glPopMatrix();							//ripristino il disegno attorno al Sole
	
	Plutone();

	glPopMatrix();
	
	fps();

	glutSwapBuffers();
	glFlush();
}


//MAIN

int main(int argc,char **argv){
	
	glutInit(&argc,argv);
	
	//disegno una finestra 1280x800 con posizione iniziale 10,10
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	
	glutInitWindowPosition(10,10);
	glutInitWindowSize(1280,800);
	
	//crea finestra principale
	finestraGen = glutCreateWindow("Sistema Solare");		//nome finestra
	
	//la finestra principale si apre in fullscreen
	glutFullScreen();
	

	init();
	glutDisplayFunc(sistemaSolare);							//lancio render
	glutReshapeFunc(reshape);
    
	
	//call menù finestra principale
	popUpMenu();

    legenda = glutCreateSubWindow(finestraGen,50,700,widthL,heightL);
    glutDisplayFunc(displayLegenda);
    
	popUpMenuLegenda();
	
	glutIdleFunc(idle);
	
	glutMainLoop();
  
	return 0;
}
