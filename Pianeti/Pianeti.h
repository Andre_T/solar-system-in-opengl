#include <cstdlib>
#include <cstdio>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include "Funzioni/NomiPianeti.h"
#include "Funzioni/Materiali.h"
#include "CGUtils/include/tga_io.h"
#include "CGUtils/include/3dmath.h"
#include <string>
#include <iostream>
#include <string.h>
#include <ctime>
#include <cmath>

extern float anno, giorno;			//variabili per la modifica della velocità di animazione

//flag per la visualizzazione dei nomi
extern bool nomi_flag, legenda_flag;

//disegno il Sole
void Sole(){

	glColor3f(1.0,0.5,0.5);
	glRotatef(giorno/25,0.0,1.0,0.0);	//rivoluzione del Sole
	if (nomi_flag)
		nomeSole();
	materialeSole();
	glutSolidSphere(5,24,24);
}

//disegno Mercurio
void Mercurio(){

	glColor3f(0.0,0.0,1.0);
	glRotatef(anno,0.0,1.0,0.0);    //rivoluzione attorno a Y, quindi attorno al Sole
	glTranslatef(-6.5,0.0,20.0);
	glPushMatrix();                 //posso modificare Mercurio
	if (nomi_flag)
		nomeMercurio();
	glRotatef(giorno,0.0,0.0,1.0);  //rivoluzione di Mercurio attorno al proprio asse
	
	materialeVenere();				//uguale a quello di Venere
	glutSolidSphere(0.4,16,16);
}

//disegno la Terra
void Terra(){

	glColor3f(0.0,0.0,1.0);
	glRotatef(anno,0.0,1.0,0.0);    //rivoluzione attorno a Y, quindi attorno al Sole
	glTranslatef(11.0,0.0,0.0);
	glPushMatrix();                 //posso modificare la Terra
	if (nomi_flag)
		nomeTerra();
	glRotatef(giorno,0.0,0.0,1.0);  //rivoluzione della Terra attorno al proprio asse
	
	materialeTerra();
	glutSolidSphere(1.4,16,16);

}

//disegno la Luna
void lunaTerra(){

	glColor3f(1.0,1.0,1.0);
	glRotatef(anno*(365/29),0.0,0.0,1.0);    //rivoluzione attorno alla Terra
	glTranslatef(0.0,2.0,0.0);
	
	materialeLune();
	glutSolidSphere(0.3,16,16);
	
}

//disegno Venere
void Venere(){

	glColor3f(0.0,0.0,1.0);
	glRotatef(anno,0.0,1.0,0.0);    //rivoluzione attorno a Y, quindi attorno al Sole
	glTranslatef(-8.5,0.0,-15.0);
	glPushMatrix();                 //posso modificare Venere
	if (nomi_flag)
		nomeVenere();
	glRotatef(giorno,0.0,0.0,1.0);  //rivoluzione di Venere attorno al proprio asse
	
	materialeVenere();
	glutSolidSphere(0.9,16,16);
}

//disegno Marte
void Marte(){
	
	glColor3f(0.0,0.0,1.0);
	glRotatef(anno,0.0,1.0,0.0);    //rivoluzione attorno a Y, quindi attorno al Sole
	glTranslatef(14.0,0.0,10.0);
	glPushMatrix();                 //posso modificare Marte
	if (nomi_flag)
		nomeMarte();
	glRotatef(giorno,0.0,0.0,1.0);  //rivoluzione della Marte attorno al proprio asse
	
	materialeMarte();
	glutSolidSphere(0.6,16,16);
}

//disegno Plutone
void Plutone(){

	glColor3f(0.0,0.0,1.0);
	glRotatef(anno,0.0,1.0,0.0);			//rivoluzione attorno a Y, quindi attorno al Sole
	glTranslatef(-35.0,-4.0,-12.0);
	glPushMatrix();							//posso modificare Plutone
	if (nomi_flag)
		nomePlutone();
	glRotatef(giorno,0.0,0.0,1.0);			//rivoluzione di Plutone attorno al proprio asse
	
	materialePlutone();
	glutSolidSphere(0.9,16,16);
}

//disegno Saturno
void Saturno(){

	glColor3f(0.65,0.57,0.57);
	glRotatef(anno,0.0,0.0,1.0);
	glTranslatef(-27,-5.0,0.0);
	glPushMatrix();							//posso modificare Saturno
	if (nomi_flag)
		nomeSaturno();
	glRotatef(giorno*(24/10),0.0,1.0,0.0);
	glRotated(90, 1.0, 0.0, 0.0);
	
	materialeSaturno();
	glutSolidSphere(2.5,16,16);
}

//disegna gli anelli di Saturno
void anelli(){
	
	int s = 30;
	glColor3f(1.0,1.0,1.0);
	glutWireTorus(0.0,3.5,s,s);
	glutWireTorus(0.0,3.55,s,s);
	glutWireTorus(0.0,3.6,s,s);
	glutWireTorus(0.0,3.65,s,s);
	glutWireTorus(0.0,3.7,s,s);
	glutWireTorus(0.0,3.75,s,s);
	glutWireTorus(0.0,3.8,s,s);
	glutWireTorus(0.0,3.85,s,s);
	glutWireTorus(0.0,3.9,s,s);
	glutWireTorus(0.0,3.95,s,s);
	glutWireTorus(0.0,4.0,s,s);
	glutWireTorus(0.0,4.2,s,s);
	glutWireTorus(0.0,4.3,s,s);
	glutWireTorus(0.0,4.5,s,s);
	glutWireTorus(0.0,4.55,s,s);
	glutWireTorus(0.0,4.6,s,s);
	glutWireTorus(0.0,4.65,s,s);
	glutWireTorus(0.0,4.7,s,s);
	glutWireTorus(0.0,4.75,s,s);
	glutWireTorus(0.0,4.8,s,s);
	glutWireTorus(0.0,4.85,s,s);
	glutWireTorus(0.0,4.9,s,s);
	glutWireTorus(0.0,4.95,s,s);
	glutWireTorus(0.0,5.0,s,s);
	
}


//disegno Giove e le sue lune
void Giove(){
	
	glColor3f(1.0,0.0,0.0);
	glRotatef(30,0.0,0.0,1.0);
	glRotatef(anno,0.0,1.0,0.0);		//rotazione di Giove attorno al Sole
	glTranslatef(25.0,0.0,0.0);
	glPushMatrix();							//posso modificare Giove
	glPushMatrix();
	glPushMatrix();
	if (nomi_flag)
		nomeGiove();
	glRotatef(giorno*(24/10),1.0,0.0,0.0);    //rotazione di Giove attorno al proprio asse
	
	materialeGiove();
	glutSolidSphere(3.8,16,16);
	
}
void lunaGiove1(){
	
	glColor3f(1.0,1.0,1.0);
	glRotatef(anno*(365/60),1.0,0.0,0.0);    //rivoluzione attorno a Giove
	glTranslatef(0.0,0.0,4.0);
	
	materialeLune();
	glutSolidSphere(0.3,16,16);
}

void lunaGiove2(){

	glColor3f(1.0,1.0,1.0);
	glRotatef(anno*(365/60),1.0,0.0,0.0);    //rivoluzione attorno a Giove
	glTranslatef(-5.0,0.0,0.0);
	
	materialeLune();
	glutSolidSphere(0.3,16,16);
}

void lunaGiove3(){
	//disegno luna di Giove
	glColor3f(1.0,1.0,1.0);
	glRotatef(anno*(365/60),1.0,0.0,0.0);    //rivoluzione attorno a Giove
	glTranslatef(3.0,0.0,-4.0);
	
	materialeLune();
	glutSolidSphere(0.3,16,16);
}
