#include "mtl_io.h"
#include "string_utils.h"
#include <fstream>
#include <stdexcept>
#include <vector>

const size_t BUFFER_SIZE=1024;

using namespace std;

namespace CGU {
  void load_mtl(const std::string& filename, 
		std::map<std::string, Material>& materials)
  {
    std::map<std::string, Material> out;

    ifstream file;
    char line[BUFFER_SIZE];  

    file.open(filename.c_str(), ifstream::in);

    if (file.fail())    
      throw std::runtime_error("Error opening MTL file.");


    bool newmtl=false;
    Material current_material;
    string current_material_name;
  
    //ciclo fra le righe del file
    while (file.getline(line, BUFFER_SIZE))
      {
	vector<string> words;

	split(string(line),
	      " ",
	      words,
	      false);   
      
	if(words.size()==0)
	  continue;

	if (words[0][0]=='#')
	  continue;
      
	if (words[0] == "newmtl")
	  {
	  if (words.size() != 2)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (newmtl)
	    {
	      if (out.count(words[1]) != 0) 
		throw std::runtime_error("material already defined!");	    
	      out[current_material_name] = current_material;
	      current_material_name = words[1];	      	      
	    }
	  else 
	    {
	      //� il primo newmtl!
	      current_material_name = words[1];
	      newmtl=true;
	    }
	  }
	else if (words[0] == "Ka") 
	  {
	  if (words.size() != 4)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else	  
	    current_material.ambient=RGBA(str_conv<float>(words[1]),
					  str_conv<float>(words[2]),
					  str_conv<float>(words[3]),
					  1.0);
	  }
	else if (words[0] == "Kd")
	  {
	  if (words.size() != 4)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else 
	    current_material.diffuse=RGBA(str_conv<float>(words[1]),
					  str_conv<float>(words[2]),
					  str_conv<float>(words[3]),
					  1.0);      
	  }

	else if (words[0] == "Ks")
	  {
	  if (words.size() != 4)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else 
	    current_material.specular=RGBA(str_conv<float>(words[1]),
					   str_conv<float>(words[2]),
					   str_conv<float>(words[3]),
					   1.0);
	  }
	else if (words[0] == "Ns")	  
	  {
	  if (words.size() != 2)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else 
	    current_material.shininess=str_conv<float>(words[1]);      	  
	  }
	else if (words[0] == "illum")
	  {
	  if (words.size() != 2)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else 
	    current_material.illum=str_conv<int>(words[1]);      
	  }

	else if (words[0] == "Tr")
	  {
	  if (words.size() != 2)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else 
	    current_material.Tr=str_conv<float>(words[1]);
	  }

	else if (words[0] == "map_Kd")
	  {
	  if (words.size() != 2)
	    throw std::runtime_error("Unvalid mtl file.");
	  else if (!newmtl) 
	    throw std::runtime_error("Unvalid mtl file.");
	  else 
	    current_material.map_Kd=words[1];      	    	  
	  }
      } //lines loop   

    //salvo nella map l'ultimo materiale
    if(newmtl)
      out[current_material_name] = current_material;

    out.swap(materials);
  } //load_mtl
}
