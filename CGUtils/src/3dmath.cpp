#include "3dmath.h"
#include <cmath>

namespace CGU {

  void mul(const Matrix4x4& m1, const Matrix4x4& m2, Matrix4x4& out)
  {
    assert((&m1!=&m2) && (&m2!=&out));

    for (int r=0; r<4; r++)
      for (int c=0; c<4; c++)
	out[c][r] = 
	  m1[0][r] * m2[c][0] +
	  m1[1][r] * m2[c][1] +
	  m1[2][r] * m2[c][2] +
	  m1[3][r] * m2[c][3];	  
  }

  void buildCameraMatrix(const Camera& c, Matrix4x4& m)
  {
    Matrix4x4 t;
    t.set(1,0,0,-c.position.x,
	  0,1,0,-c.position.y,
	  0,0,1,-c.position.z,
	  0,0,0,1);
  
    Matrix4x4 f;	      
    f.set(c.frame.Xaxis.x, c.frame.Xaxis.y, c.frame.Xaxis.z, 0,
	  c.frame.Yaxis.x, c.frame.Yaxis.y, c.frame.Yaxis.z, 0,
	  c.frame.Zaxis.x, c.frame.Zaxis.y, c.frame.Zaxis.z, 0,
	  0,0,0,1);
  
    mul(f,t,m);
  }

  void transform (const Matrix4x4& m, const vec3& v, vec3& out)
  {
    assert(&v!=&out);
    out.x=m[0][0]*v.x + m[1][0]*v.y + m[2][0]*v.z + m[3][0];
    out.y=m[0][1]*v.x + m[1][1]*v.y + m[2][1]*v.z + m[3][1];
    out.z=m[0][2]*v.x + m[1][2]*v.y + m[2][2]*v.z + m[3][2];
  }

  void transform (const Matrix4x4& m, vec3& out)
  {
    vec3 t(out);
    out.x=m[0][0]*t.x + m[1][0]*t.y + m[2][0]*t.z + m[3][0];
    out.y=m[0][1]*t.x + m[1][1]*t.y + m[2][1]*t.z + m[3][1];
    out.z=m[0][2]*t.x + m[1][2]*t.y + m[2][2]*t.z + m[3][2];
  }

  void buildRotationMatrix(const vec3& pv, float angle, Matrix4x4& m)
  {
    float c = cos(angle);
    float s = sin(angle);
  
    vec3 v(pv);
    normalize(v);

    m.set( v.x*v.x*(1-c) + c, v.x*v.y*(1-c)-v.z*s,  v.x*v.z*(1-c)+v.y*s, 0,
	   v.y*v.x*(1-c)+v.z*s, v.y*v.y*(1-c)+c, v.y*v.z*(1-c)-v.x*s, 0,
	   v.x*v.z*(1-c)-v.y*s, v.y*v.z*(1-c)+v.x*s, v.z*v.z*(1-c)+c, 0,  
	   0, 0, 0, 1);
  }


  void normalize(vec3& v)
  {
    float length = v.x*v.x + v.y*v.y + v.z*v.z;
    assert (length != 0);
    length = pow(length, 0.5);  
    v.x /= length;
    v.y /= length;
    v.z /= length;
  }

  float length(const vec3& v)
  {
    return pow(v.x*v.x + v.y*v.y + v.z*v.z, 0.5);
  }


  void cross_product(const vec3& v1, const vec3& v2, vec3& v)
  {
    assert((&v != &v1) && (&v != &v2));
    v.x = v1.y * v2.z - v1.z * v2.y;
    v.y = v1.z * v2.x - v1.x * v2.z;
    v.z = v1.x * v2.y - v1.y * v2.x;
  }

}
