#include "obj_io.h"
#include "string_utils.h"
#include <fstream>
#include <stdexcept>
#include <vector>

#include <algorithm>
#include <iterator>

static const size_t BUFFER_SIZE=1024;

using namespace std;

inline void decode_face_vertex(const string& word, 
			       int& v, int& t, int& n,
			       const int& v_size, const int& t_size, const int& n_size)
{
  vector<string> vert;
  CGU::split(word,
	     "/",
	     vert,
	     true);   

  if ((vert.size()==2) || (vert.size()==0) || (vert.size()>5) )    
    throw std::runtime_error("Unvalid obj file.");
    
  if (vert.size()==1)
    {
      v = CGU::str_conv<int>(vert[0]);
      v = (v < 0) ? v_size + v: v - 1;
      if ((v > v_size) || (v < 0))
	throw std::runtime_error("Illegal indexes");
      t = -1;
      n = -1;
    }
  
  else if (vert.size()==3)
    {
      v = CGU::str_conv<int>(vert[0]);
      v = (v < 0) ? v_size + v : v - 1;
      if ((v > v_size) || (v < 0))
	throw std::runtime_error("Illegal indexes");
      t = CGU::str_conv<int>(vert[2]);
      t = (t < 0) ? t_size + t : t - 1;
      if ((t > t_size) || (t < 0))
	throw std::runtime_error("Illegal indexes");
      n = -1;
    }
  else if (vert.size()==4)
    {
      v = CGU::str_conv<int>(vert[0]);
      v = (v < 0) ? v_size + v : v - 1;
      if ((v > v_size) || (v < 0))
	throw std::runtime_error("Illegal indexes");
      t = -1;
      n = CGU::str_conv<int>(vert[3]);
      n = (n < 0) ? n_size + n : n - 1;
      if ((n > n_size) || (n < 0))
	throw std::runtime_error("Illegal indexes");
    }
  else if (vert.size()==5)
    {
      v = CGU::str_conv<int>(vert[0]);
      v = (v < 0) ? v_size + v : v - 1;
      if ((v > v_size) || (v < 0))
	throw std::runtime_error("Illegal indexes");
      t = CGU::str_conv<int>(vert[2]);
      t = (t < 0) ? t_size + t : t - 1;
      if ((t > t_size) || (t < 0))
	throw std::runtime_error("Illegal indexes");
      n = CGU::str_conv<int>(vert[4]);
      n = (n < 0) ? n_size + n : n - 1;
      if ((n > n_size) || (n < 0))
	throw std::runtime_error("Illegal indexes");
    }
}

namespace CGU {
  void load_obj(const std::string& filename,
		material_list& materials,
		std::string& material,
		TriMesh& trimesh)
  {
    material_list mat_out;
 
    ifstream file;
    char line[BUFFER_SIZE];  

    file.open(filename.c_str(), ifstream::in);

    if (file.fail())    
      throw std::runtime_error("Error opening OBJ file.");

    bool mtllib=false;
    bool usemtl=false;
    string current_material_name;
    TriMesh current_trimesh;
    int line_num=0;
    int v1,t1,n1;
    int v2,t2,n2;
    int v3,t3,n3;

    try {
      //ciclo fra le righe del file
      while (file.getline(line, BUFFER_SIZE))
	{
	  ++line_num;
	  vector<string> words;
	  string str_line(line);
	  replace_all(str_line, "\t", " ");
	  trim(str_line);	
	
	  if(str_line.length()==0)
	    continue;
	
	  split(str_line,
		" ",
		words,
		false);   

	  if (words[0][0]=='#')
	    continue;
      
	  if (words[0] == "mtllib")      
	    if (words.size() != 2)
	      throw std::runtime_error("Unvalid OBJ file.");
	    else 
	      {
		//effettuo il parsing dell'mtl
		material_list temp_mat;
		load_mtl(words[1], temp_mat);
		mat_out.insert(temp_mat.begin(), temp_mat.end());
		mtllib=true;
	      }
	  else if(words[0] == "usemtl")
	    if (!mtllib)
	      throw std::runtime_error("No mtllib loaded before usemtl instance.");
	    else if (words.size() != 2)
	      throw std::runtime_error("Unvalid OBJ file.");
	    else if (mat_out.count(words[1]) == 0)
	      throw std::runtime_error("Undefined material.");
	    else if (usemtl)
	      throw std::runtime_error("Only one Material is permitted.");
	    else	    
	      {	      
		current_material_name=words[1];
		usemtl=true;
	      }
	  
	  else if (words[0] == "v")
	    if (words.size() != 4)
	      throw std::runtime_error("Unvalid obj file.");
	    else	  
	      current_trimesh.vertex_push_back(vec3(CGU::str_conv<float>(words[1]),
						    CGU::str_conv<float>(words[2]),
						    CGU::str_conv<float>(words[3])));
	  else if (words[0] == "vn")
	    if (words.size() != 4)
	      throw std::runtime_error("Unvalid obj file.");
	    else	  
	      current_trimesh.normal_push_back(vec3(CGU::str_conv<float>(words[1]),
						    CGU::str_conv<float>(words[2]),
						    CGU::str_conv<float>(words[3])));
	  else if (words[0] == "vt")
	    if (words.size() != 3)
	      throw std::runtime_error("Unvalid obj file.");
	    else
	      current_trimesh.texCoords_push_back(vec2(CGU::str_conv<float>(words[1]),
						       CGU::str_conv<float>(words[2])));
	
	  else if (words[0] == "f")	
	    if (words.size() != 4)
	      throw std::runtime_error("Only triangles are supported.");
	    else
	      {
		decode_face_vertex(words[1],v1,t1,n1, 
				   current_trimesh.vertex_size(),
				   current_trimesh.texCoords_size(),
				   current_trimesh.normal_size());

		decode_face_vertex(words[2],v2,t2,n2,	    
				   current_trimesh.vertex_size(),
				   current_trimesh.texCoords_size(),
				   current_trimesh.normal_size());

		decode_face_vertex(words[3],v3,t3,n3,
				   current_trimesh.vertex_size(),
				   current_trimesh.texCoords_size(),
				   current_trimesh.normal_size());

		current_trimesh.face_push_back(TriMesh::Face(v1,n1,t1,
							     v2,n2,t2,
							     v3,n3,t3));	    	      	      
	      }
	  else if ( (words[0] != "g") &&
		    (words[0] != "o") ) 	  
	    throw std::runtime_error(string("Usupported or unvalid obj tag: ") + words[0]);
	  
           	    	  
	} //lines loop
    } catch(exception& e)
      {
	throw std::runtime_error(string("(obj line ").append(to_str(line_num)).
				 append(") ").append(line).append(": ").append(e.what()));
      }

    //ritorno in output materiali e mesh
    if(usemtl)
      {
	material=current_material_name;
	trimesh.swap(current_trimesh);
      }
    else 
      {
	mat_out["__default__"] = Material();	
	material="__default__";
	trimesh.swap(current_trimesh);	    
      }
        
    mat_out.swap(materials);
  }
}
