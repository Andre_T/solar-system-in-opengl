#include "string_utils.h"

using namespace std;

namespace CGU {
  //------------------------------------------------------
  int replace_all(std::string& input,
		  const std::string& what,
		  const std::string& with)
  {
    size_t pos=0;
    int occurrences=0;
    while ( (pos=input.find(what, pos)) != std::string::npos)
      {
	input.erase(pos, what.size());
	input.insert(pos, with);
	++occurrences;
      }   
    return occurrences;
  }

  //------------------------------------------------------
  void trim(string& str, char space)
  {
    string::size_type pos = str.find_last_not_of(space);
    if(pos != string::npos) {
      str.erase(pos + 1);
      pos = str.find_first_not_of(' ');
      if(pos != string::npos) str.erase(0, pos);
    }
    else str.erase(str.begin(), str.end());
  }

  //------------------------------------------------------
  int split(const string& input, 
	    const string& delimiter, 
	    vector<string>& results, 
	    bool includeEmpties)
  {
    int del_size=delimiter.size();
    int in_size=input.size();
    int startPos, newPos;
    int numFound=0;
    string s;

    if( ( del_size == 0 ) || ( in_size == 0 ))    
      {
	results.push_back(input);
	return 0;    
      }
  
    startPos=0;
    newPos = input.find (delimiter, startPos);
  
    if( newPos < 0 )
      { 
	results.push_back(input);
	return 0;       
      }
  
    while( newPos >= startPos )
      {
	++numFound;
       
	if ( (newPos-startPos) > 0)
	  results.push_back(input.substr(startPos, newPos-startPos));
       
	if (includeEmpties)	 
	  results.push_back(delimiter);
       
	startPos = newPos + del_size;
	newPos = input.find (delimiter, startPos);       
      }

    if (startPos < in_size)
      results.push_back(input.substr(startPos, newPos-startPos));
   
    return numFound;   
  }

} //namespace CGU
