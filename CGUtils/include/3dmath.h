#ifndef _3DMATH_H
#define _3DMATH_H

#include <cassert>
#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265
#endif

namespace CGU {

  // Tipi ---------------------------------------------------------
  /**
   * Un vettore/punto/normale 3D
   */
  class vec3 {
  public:
    float x;
    float y;
    float z;
  
    vec3() {}
  
    vec3(float px, float py, float pz)    
    {
      set(px,py,pz);
    }

    void set(float px, float py, float pz)
    {
      x=px;
      y=py;
      z=pz;
    }    
  };

  /**
   * Vettore/punto 2D. Utile per rappresentrare coordinate di texture
   * 2D.
   */
  class vec2 {
  public:
    float s;
    float t;
  
    vec2() {}
  
    vec2(float ps, float pt)    
    {
      set(ps,pt);
    }

    void set(float ps, float pt)
    {
      s=ps;
      t=pt;
    }    
  };
  //-----------------------------------------------------------------

  /**
   * Una semplice classe per una matrice 4x4 in coordinate omogenee.
   * Gli elemnti possono essere acceduti tramite l'operatore
   * m[colonna][riga]. La memorizzazione interna dei dati � equivalente
   * a quella richiesta dalle operazioni OpenGL quali glLoadMatrix,
   * glMultMatrix etc..
   */
  class Matrix4x4 
  {
  private:
    float m[16];

  public:
  
    void set(float m00, float m01, float m02, float m03,
	     float m10, float m11, float m12, float m13,
	     float m20, float m21, float m22, float m23,
	     float m30, float m31, float m32, float m33)
    {
      m[0] = m00; m[1] = m10; m[2] = m20; m[3] = m30;
      m[4] = m01; m[5] = m11; m[6] = m21; m[7] = m31;
      m[8] = m02; m[9] = m12; m[10] = m22; m[11] = m32;
      m[12] = m03; m[13] = m13; m[14] = m23; m[15] = m33;    
    }

    void setIdentity() 
    {
      set(1,0,0,0,
	  0,1,0,0,
	  0,0,1,0,
	  0,0,0,1);
    }

    const float* GL_array() const
    {
      return &m[0];
    }
  
    float* operator[](unsigned column)
    {
      assert(column<4);
      return &m[column*4];
    }
  
    const float* operator[](unsigned column) const
    {
      assert(column<4);
      return &m[column*4];
    }
  };
  //-----------------------------------------------------------------

  /**
   * Questa classe rappresenta un frame di assi ortonormale.
   */
  class OrthonormalFrame 
  {
  
  public:
    vec3 Xaxis;
    vec3 Yaxis;
    vec3 Zaxis;
  
    OrthonormalFrame()
    {
      Xaxis.set(1,0,0);
      Yaxis.set(0,1,0);
      Zaxis.set(0,0,1);
    }

    OrthonormalFrame(vec3 xa, vec3 ya, vec3 za)
    {
      set(xa, ya, za);    
    }

    OrthonormalFrame(float xa_x, float xa_y, float xa_z,
		     float ya_x, float ya_y, float ya_z,
		     float za_x, float za_y, float za_z)
    {
      set(xa_x, xa_y, xa_z,
	  ya_x, ya_y, ya_z,
	  za_x, za_y, za_z);
    }

    void set(vec3 xa, vec3 ya, vec3 za)
    {
      Xaxis=xa;
      Yaxis=ya;
      Zaxis=za;
    }

    void set(float xa_x, float xa_y, float xa_z,
	     float ya_x, float ya_y, float ya_z,
	     float za_x, float za_y, float za_z)
    {
      Xaxis.set(xa_x,  xa_y,  xa_z);
      Yaxis.set(ya_x,  ya_y,  ya_z);
      Zaxis.set(za_x,  za_y,  za_z);
    }
  };

  /**
   * Questa classe rappresenta una camera tramite una posizione e un
   * frame di assi ortonormali.
   */
  class Camera
  {
  public:
    vec3 position;
    OrthonormalFrame frame;
  
    Camera() : position(0,0,0), frame() {}
  };

  //- Funzioni --------------------------------------------------------

  /**
   * Questa funzione normalizza il vettore in ingresso.
   */
  void normalize(vec3& v);

  /**
   * Calcola la norma euclidea del vettore in input
   */
  float length(const vec3& v);

  /**
   * Questa funzione calcola il prodotto vettoriale (cross product) tra
   * due vettori in 3 dimensioni.
   */ 
  void cross_product(const vec3& v1, const vec3& v2, vec3& v);

  /**
   * Somma tra due vec3
   */
  inline void add(const vec3& v1, const vec3& v2, vec3& out)
  {
    out.x = v1.x + v2.x;
    out.y = v1.y + v2.y;
    out.z = v1.z + v2.z;
  }

  /**
   * Sottrazione tra due vec3
   */
  inline void sub(const vec3& v1, const vec3& v2, vec3& out)
  {
    out.x = v1.x - v2.x;
    out.y = v1.y - v2.y;
    out.z = v1.z - v2.z;
  }

  /**
   * moltiplicazione tra matrici 4x4
   */
  void mul(const Matrix4x4& m1, const Matrix4x4& m2, Matrix4x4& out);

  /**
   * Data una camera, costruisce la matrice 4x4 di trastormazione che
   * effettua il cambiamento di base da world space e camera space.
   */
  void buildCameraMatrix(const Camera& c, Matrix4x4& m);

  /**
   * Trasforma il vertice @v per la matrice @m.
   */
  void transform (const Matrix4x4& m, const vec3& v, vec3& out);

  /**
   * Trasforma il vertice @v per la matrice @m (versione in-place)
   */
  void transform (const Matrix4x4& m, vec3& out);

  /**
   * Costruisce la matrice di rotazione 4x4 attorno al vettore v con un angolo angle.
   * @param v il vettore di rotazione. 
   * @param angle l'angolo di rotazione in radianti
   * @param m la matrice risultante
   */
  void buildRotationMatrix(const vec3& pv, float angle, Matrix4x4& m);

  /**
   * Conversione gradi -> radianti
   */
  inline float deg2rad(float deg)
  {
    return 0.0174532925 * deg;
  }

  /**
   * Calcola la sfera di contenimento minima di una nuvola di punti.
   * @param begin iteratore al primo elemento di una sequenza di vec3
   * @param begin iteratore all'elemento successivo all'ultimo della sequenza
   * @param centroid centroide della nuvola
   * @param raggio della sfera
   */ 
  template <typename iterT>
  void compute_bounding_sphere(iterT begin, 
			       iterT end, 
			       vec3& centroid, 
			       float& radius)
  {
    centroid.set(0,0,0);
    iterT it=begin;
    unsigned counter=0;
  
    while (it!=end)
      {
	add(centroid, *it, centroid);
	counter++;
	it++;
      }
  
    centroid.x/=counter;
    centroid.y/=counter;
    centroid.z/=counter;

    it=begin;
    radius = 0;
    vec3 temp;
    float len;
  
    while (it!=end)
      {
	sub(*it, centroid, temp);
	len=length(temp);
	if (len > radius)
	  radius = len;
	it++;
      }  
  }
} //namespace CGU
#endif //_3DMATH_H
