/** Classe TGAImg; basata sul codice:
 * http://gpwiki.org/index.php/LoadTGACpp 
 * Modificata da Alessandro Colombo 
 * per il corso di Elementi di Informatica Grafica. 
 * TGA Loader - 16/11/04 Codehead
 */
#ifndef TGA_IO_H
#define TGA_IO_H 

#include <string>

namespace CGU {

  class TGAImg
  {
  public:
    TGAImg();
    ~TGAImg();
  
    /**
     * Load a TGA image form the file located at the specified path.
     */
    void load(const std::string& szFilename);

    /**
     * @return Bits Per Pixel
     */
    int BPP() const;

    /**
     * @return image width
     */
    int width() const;
    /**
     * @return image height
     */
    int height() const;
    /**
     * @return a pointer to image data
     */
    //const unsigned char* pixels() const;       
    /**
     * @return a pointer to image data
     */
    const unsigned char* pixels() const;
    /**
     * @return a pointer to VGA palette
     */
    //unsigned char* palette();   
    /**
     * @return a pointer to VGA palette
     */
    const unsigned char* palette() const;
 
  private:  
    TGAImg& operator=(const TGAImg&);
    TGAImg(const TGAImg& img);

    short int iWidth,iHeight,iBPP;
    unsigned long lImageSize;
    char bEnc;
    unsigned char *pImage, *pPalette, *pData;
   
    // Internal workers
    void ReadHeader();
    void LoadRawData();
    void LoadTgaRLEData();
    void LoadTgaPalette();
    void BGRtoRGB();
    void FlipImg();
  };

} //namespace CGU
#endif // TGA_IO_H
