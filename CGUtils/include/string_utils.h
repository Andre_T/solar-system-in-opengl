#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>
#include <vector>
#include <sstream>
#include <stdexcept>

namespace CGU {

  //- Functions declaration ---------------------------
  void trim(std::string& str, char space=' ');

  int split(const std::string& input, 
	    const std::string& delimiter, 
	    std::vector<std::string>& results, 
	    bool includeEmpties);

  int replace_all(std::string& input,
		  const std::string& what,
		  const std::string& with);

  template<typename T>
    T str_conv(const std::string& s);

  template<typename T>
    std::string str_conv(const T&);


  //- Implementation of template functions -----------
  template<typename T>
    T str_conv(const std::string& s)
    {
      T value;
      std::istringstream iss(s);

      if(!(iss >> value))
	throw std::runtime_error("String conversion not possible.");
      else
	return value;
    }

  template<typename T>
    std::string to_str(const T& d)
    {
      std::ostringstream oss;
  
      if(!(oss << d))
	throw std::runtime_error("Conversion to string not possible.");
      else
	return oss.str();
    }
} //namespace CGU
#endif //STRING_UTILS_H
