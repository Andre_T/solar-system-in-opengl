#ifndef TRIMESH_H
#define TRIMESH_H

#include "3dmath.h"
#include <vector>

namespace CGU {
  class TriMesh {
  
  public:
    typedef int size_type;   ///< type of dimensions and indices

    class Face {	
    
    public:
      
      Face() {}

      Face(size_type pv1, size_type pn1, size_type pt1,
	   size_type pv2, size_type pn2, size_type pt2,
	   size_type pv3, size_type pn3, size_type pt3)
	{		   
	  v1=pv1;
	  v2=pv2;
	  v3=pv3;

	  n1=pn1;
	  n2=pn2;
	  n3=pn3;

	  t1=pt1;
	  t2=pt2;
	  t3=pt3;
	}
                    
      size_type v1, n1, t1;
      size_type v2, n2, t2;
      size_type v3, n3, t3;
    };
    
    typedef std::vector<Face> face_buffer_type;
    typedef std::vector<vec3> vertex_buffer_type;
    typedef std::vector<vec3> normal_buffer_type;
    typedef std::vector<vec2> texCoords_buffer_type;

    typedef face_buffer_type::iterator face_iterator; ///< type of face iterator    
    typedef face_buffer_type::const_iterator const_face_iterator; ///< type of face const iterator
  
    typedef vertex_buffer_type::iterator vertex_iterator; ///< type of vertex iterator    
    typedef vertex_buffer_type::const_iterator const_vertex_iterator; ///< type of vertex const iterator

    typedef normal_buffer_type::iterator normal_iterator; ///< type of normal iterator    
    typedef normal_buffer_type::const_iterator const_normal_iterator; ///< type of normal const iterator

    typedef texCoords_buffer_type::iterator texCoords_iterator; ///< type of texCoords iterator    
    typedef texCoords_buffer_type::const_iterator const_texCoords_iterator; ///< type of texCoords const iterator
  

  TriMesh() : mFaces(0), mVertexes(0), mNormals(0), mTexCoords(0) {};

  TriMesh(size_type nFaces, 
	  size_type nVert, 
	  size_type nNorm, 
	  size_type nTex) : mFaces(nFaces), mVertexes(nVert), mNormals(nNorm), mTexCoords(nTex) {};

    ~TriMesh() {}




    //--- Methods ---
    /**
     * @return texCoords list begin iterator
     */
    inline texCoords_iterator texCoords_begin() {return mTexCoords.begin();}

    /**
     * @return texCoords list end iterator
     */
    inline texCoords_iterator texCoords_end() {return mTexCoords.end();}

    /**
     * @return texCoords list begin const iterator
     */
    inline const_texCoords_iterator texCoords_begin() const {return mTexCoords.begin();}

    /**
     * @return texCoords list end const iterator
     */
    inline const_texCoords_iterator texCoords_end() const {return mTexCoords.end();}

    /**
     * @return vertex list begin iterator
     */
    inline vertex_iterator vertex_begin() {return mVertexes.begin();}

    /**
     * @return vertex list end iterator
     */
    inline vertex_iterator vertex_end() {return mVertexes.end();}

    /**
     * @return vertex list begin const iterator
     */
    inline const_vertex_iterator vertex_begin() const {return mVertexes.begin();}

    /**
     * @return vertex list end const iterator
     */
    inline const_vertex_iterator vertex_end() const {return mVertexes.end();}

    /**
     * @return face list begin iterator
     */
    inline face_iterator face_begin() {return mFaces.begin();}

    /**
     * @return face list end iterator
     */
    inline face_iterator face_end()  {return mFaces.end();}

    /**
     * @return face list begin const iterator
     */
    inline const_face_iterator face_begin() const {return mFaces.begin();}

    /**
     * @return face list end const iterator
     */
    inline const_face_iterator face_end() const {return mFaces.end();}    

    /**
     * @return normal list begin iterator
     */
    inline normal_iterator normal_begin() {return mNormals.begin();}

    /**
     * @return normal list end iterator
     */
    inline normal_iterator normal_end()  {return mNormals.end();}

    /**
     * @return normal list begin const iterator
     */
    inline const_normal_iterator normal_begin() const {return mNormals.begin();}

    /**
     * @return normal list end const iterator
     */
    inline const_normal_iterator normal_end() const {return mNormals.end();}    

    /**
     * Adds a face to the mesh.
     * @param v1 the vertex 1 index
     * @param v2 the vertex 2 index
     * @param v3 the vertex 3 index

     * @param n1 the normal 1 index
     * @param n2 the normal 2 index
     * @param n3 the normal 3 index

     * @param t1 the texCoords 1 index
     * @param t2 the texCoords 2 index
     * @param t3 the texCoords 3 index


     */
    face_iterator face_push_back(size_type v1, size_type v2, size_type v3,
				 size_type n1, size_type n2, size_type n3,
				 size_type t1, size_type t2, size_type t3) {				 	
      mFaces.push_back(Face(v1, v2 ,v3, n1, n2, n3, t1, t2, t3));
      return --mFaces.end();	//pointer at the pushed element
    }

    /**
     * Adds a face to the mesh      
     */
    face_iterator face_push_back(const Face& f) {
      mFaces.push_back(f);
      return --mFaces.end();
    }

    /**
     * Adds a vertex to the mesh      
     * @return the vertex index
     */
    size_type vertex_push_back(const vec3& v) {
      mVertexes.push_back(v);
      return mVertexes.size()-1;
    }

    /**
     * Adds a normal to the mesh      
     * @return the normal index
     */
    size_type normal_push_back(const vec3& v) {
      mNormals.push_back(v);
      return mNormals.size()-1;
    }

    /**
     * Adds a normal to the mesh      
     * @return the normal index
     */
    size_type texCoords_push_back(const vec2& v) {
      mTexCoords.push_back(v);
      return mTexCoords.size()-1;
    }
  
    
    /**
     * @return the number of vertexes of the mesh
     */
    inline size_type vertex_size(void) const {return mVertexes.size();}

    /**
     * @return the number of faces of the mesh
     */    
    inline size_type face_size(void) const {return mFaces.size();}

    /**
     * @return the number of normals of the mesh
     */    
    inline size_type normal_size(void) const {return mNormals.size();}

    /**
     * @return the number of texCoords of the mesh
     */    
    inline size_type texCoords_size(void) const {return mTexCoords.size();}

  
    /**
     * @return the i-th vertex     
     */
    inline vec3& vertex(size_type i) {return mVertexes[i];}
    
    /**
     * @return the i-th vertex (const version)     
     */
    inline const vec3& vertex(size_type i) const {return mVertexes[i];}


    /**
     * @return the i-th normal
     */
    inline vec3& normal(size_type i) {return mNormals[i];}
  
    /**
     * @return the i-th normal (const version)     
     */
    inline const vec3& normal(size_type i) const {return mNormals[i];}


    /**
     * @return the i-th texCoords
     */
    inline vec2& texCoords(size_type i) {return mTexCoords[i];}
  
    /**
     * @return the i-th normal (const version)     
     */
    inline const vec2& texCoords(size_type i) const {return mTexCoords[i];}


    /**
     * swap the contents of the current mesh with that of the input mesh
     */
    void swap(TriMesh& tm)
    {
      mVertexes.swap(tm.mVertexes);
      mFaces.swap(tm.mFaces);
      mNormals.swap(tm.mNormals);
      mTexCoords.swap(tm.mTexCoords);
    }

  private: 
    
    face_buffer_type mFaces;
    vertex_buffer_type mVertexes;
    normal_buffer_type mNormals;
    texCoords_buffer_type mTexCoords;  
  };

}

#endif //TRIMESH_H
