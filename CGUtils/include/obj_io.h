#ifndef OBJ_IO_H
#define OBJ_IO_H

#include "mtl_io.h"
#include <trimesh.h>
#include <string>
#include <list>

namespace CGU {
  void load_obj(const std::string& filename,
		material_list& materials,
		std::string& material,
		TriMesh& trimesh);
}
#endif
