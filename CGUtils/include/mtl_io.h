#ifndef MTL_IO_H
#define MTL_IO_H

#include <map>
#include <string>
#include "material.h"

namespace CGU {

  typedef std::map< std::string, Material > material_list;

  void load_mtl(const std::string& filename, 
		material_list& materials);
}


#endif
