#ifndef RGBA_H
#define RGBA_H

namespace CGU {
  class RGBA
  {
  public:
    float array[4];
  
    float& r() {return array[0];}
    const float& r() const {return array[0];}

    float& g() {return array[1];}
    const float& g() const {return array[1];}

    float& b() {return array[2];}
    const float& b() const {return array[2];}

    float& a() {return array[3];}
    const float& a() const {return array[3];}

    RGBA() {}
    RGBA(float pr, float pg, float pb, float pa)
      {
	set(pr,pg,pb,pa);
      }
  
    void set(float pr, float pg, float pb, float pa)
    {
      r()=pr;
      g()=pg;
      b()=pb;
      a()=pa;
    }    
  };

} //namespace CGU
#endif
