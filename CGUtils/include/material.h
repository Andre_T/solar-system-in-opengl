#ifndef MATERIAL_H
#define MATERIAL_H

#include "3dmath.h"
#include "rgba.h"
#include <string>
#include <iostream>

namespace CGU {

  class Material
  {
  public:
    RGBA ambient;
    RGBA diffuse;
    RGBA specular;
    float shininess;
    RGBA emissive;
    int illum;
    float d;
    float Tr; 
    std::string map_Kd;
 

  Material() : ambient(0.2,0.2,0.2, 1),
      diffuse(0.8, 0.8, 0.8, 1.0),
      specular(0.0,0.0,0.0,1.0),
      shininess(0),
      emissive(0,0,0,1.0),
      illum(0),
      d(0),
      Tr(0),
      map_Kd("") {}
  };

}//namespace CGU
#endif //MATERIAL_H
